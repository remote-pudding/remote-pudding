/*
 * PROGRAM:
 *   Remote-pudding
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   A program designed to play « puddi puddi » meme on a remote computer.
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/

﻿
#include "ReadConf.h"

ReadConf::ReadConf(QString path)
{
	configFile.setFileName(path);
	if(!configFile.open(QIODevice::ReadOnly | QIODevice::Text))
		throw std::runtime_error("Cannot open config file");

	QTextStream stream(&configFile);

	QString line="";

	do {
		line=stream.readLine();
		if(line.contains(QRegExp("^\\s*code\\s*=")))
		{
			line.remove(QRegExp("^\\s*code\\s*=\\s*\\D*"));
			line.remove(QRegExp("\\D*$"));
			code=(quint64)line.toInt();
		}
	} while(!line.isNull());
}

