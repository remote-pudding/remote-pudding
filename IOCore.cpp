/*
 * PROGRAM:
 *   Remote-pudding
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   A program designed to play « puddi puddi » meme on a remote computer.
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/


#include "IOCore.h"

IOCore::IOCore()
{
	server=new QTcpServer;

	lastFlags=0;
	lastCode=0;

	// DEBUG
	realCode=12345;
}

void IOCore::exec()
{
	if(!server.listen(QHostAddress::Any, 42666))
		throw std::runtime_error("Cannot start the server on port 42666.");

	connect(server, SIGNAL(newConnection()), this, SLOT(newServerConnection()));
}

void IOCore::newServerConnection() // SLOT
{
	while(server->hasPendingConnections())
	{
		QTcpSocket* socket=server->nextPendingConnection();

		connect(socket, SIGNAL(readyRead()), this, SLOT(socketReadyRead()));
	}
}

void IOCore::socketReadyRead() // SLOT
{
	QTcpSocket* sock=qobject_cast<QTcpSocket *>(sender());
	if(!sock)
		return;

	QDataStream stream(sock);

	if(lastFlags==0)
	{
		if(sock.bytesAvailable() < sizeof(quint8))
			return;
		stream >> lastFlags;
	}

	if(lastCode==0)
	{
		if(sock.bytesAvailable() < sizeof(quint64))
			return;
		stream >> lastCode;
	}

	processPacket();

	lastFlags=0;
	lastCode=0;
}

void IOCore::processPacket()
{
	if(lastCode!=realCode)
		return;

	// Exclusive actions
	if(lastFlags & NET_PLAY)
	{
		puddiControl(NET_PLAY);
	}
	else if(lastFlags & NET_STOP)
	{
		puddiControl(NET_STOP);
	}
	else if(lastFlags & NET_PAUSE)
	{
		puddiControl(NET_PAUSE);
	}

	if(lastFlags & NET_VOLUP)
	{
		puddiControl(NET_VOLUP);
	}
	else if(lastFlags & NET_VOLDOWN)
	{
		puddiControl(NET_VOLUP);
	}
}

void IOCore::puddiControl(quint8 action)
{
	switch(action)
	{
		case NET_PLAY:
			break;
		case NET_STOP:
			break;
		case NET_PAUSE:
			break;

		case NET_VOLUP:
			break;
		case NET_VOLDOWN:
			break;

		default:
			// Dafuq?
			break;
	}
}

