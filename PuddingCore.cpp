/*
 * PROGRAM:
 *   Remote-pudding
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   A program designed to play « puddi puddi » meme on a remote computer.
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/


#include "PuddingCore.h"

Pudding::Pudding()
{
	// What the fuck is this class name?!
	audioOutput=new Phonon::AudioOutput(Phonon::MusicCategory, this); // Is that REALLY music?
	mediaObject=new Phonon::MediaObject(this);

	Phonon::createPath(mediaObject,audioOutput);

	mediaObject->setCurrentSource(Phonon::MediaSource("./pdi.dll")); // Does not really looks like a DLL, huh?

	connect(mediaObject, SIGNAL(finished()), this, SLOT(playbackFinished()));
}

void Pudding::playbackFinished() /// Loop. Forever.
{
	
}

