/*
 * PROGRAM:
 *   Remote-pudding
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   A program designed to play « puddi puddi » meme on a remote computer.
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/


#ifndef DEF_CORE
#define DEF_CORE

#include <QTcpServer>
#include <QTcpSocket>
#include <QDataStream>
#include <stdexcept>

#include "data.h"

class IOCore : public QObject
{
	Q_OBJECT
	public:
		IOCore(); /// Constructor
		void exec(); /// Makes the class functionnal

	protected slots:
		void newServerConnection(); /// Called when a socket connects to the server
		void socketReadyRead(); /// Called when a socket have new data
	
	protected: // Methods
		void processPacket(); /// Processes received data (code + flags)

		void puddiControl(quint8 action);

	protected:
		QTcpServer* server;

		quint8 lastFlags;
		quint64 lastCode;

		quint64 realCode;
};

#endif//DEF_CORE

